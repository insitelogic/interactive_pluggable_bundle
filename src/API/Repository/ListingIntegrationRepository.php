<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\Repository;

use InSiteLogic\Integration\Salesforce\Listing\API\Model\Database\DBLot;
use InSiteLogic\Integration\Salesforce\Listing\API\Model\Database\DBStage;
use InSiteLogic\Integration\Salesforce\Listing\API\Proxy\IntegrationMessageProxyInterface;
use InSiteLogic\Util\ISDBAdapter;
use InSiteLogic\Util\ObjectHydrator;

class ListingIntegrationRepository {

	/**
	 * @var ISDBAdapter
	 */
	private $dbAdapter;

	/**
	 * @var IntegrationMessageProxyInterface
	 */
	private $delegatedDatabaseProxy;

	/**
	 * ListingIntegrationRepository constructor.
	 * @param mixed $dbAdapter
	 */
	public function __construct(ISDBAdapter $dbAdapter, IntegrationMessageProxyInterface $proxyInterface) {
		$this->dbAdapter = $dbAdapter;
		$this->delegatedDatabaseProxy = $proxyInterface;
	}

	public function LookupRecordByName($recordName) {
		return $this->delegatedDatabaseProxy->LookupRecordIdByName($this->dbAdapter, $recordName);
	}

	public function updateRecordWithStatus($recordId, $status) {
		return $this->delegatedDatabaseProxy->applyIntegrationMessageToDatabase($this->dbAdapter, $recordId, $status);
	}
}
