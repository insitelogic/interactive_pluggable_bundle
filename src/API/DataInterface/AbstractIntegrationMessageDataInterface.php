<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\DataInterface;

use InSiteLogic\Integration\Salesforce\Listing\API\Model\DTO\ListingMessageDTO;

abstract class AbstractIntegrationMessageDataInterface implements DataInterface {

	/**
	 * @var ListingMessageDTO
	 */
	protected $data;

	/**
	 * AbstractIntegrationMessageDataInterface constructor.
	 * @param ListingMessageDTO $data
	 */
	public function __construct(ListingMessageDTO $data) { $this->data = $data; }
}
