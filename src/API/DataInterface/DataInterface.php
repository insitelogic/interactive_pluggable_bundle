<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\DataInterface;

interface DataInterface {

	public function getListingName();

	public function interpretStatus();
}
