<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\Impl;

use InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\AbstractIntegrationMessageDataInterface;

class DefaultIntegrationMessageDataInterface extends AbstractIntegrationMessageDataInterface {

	/**
	 * @return String
	 */
	public function getListingName() {
		return $this->data->getUnitName();
	}

	/**
	 * @return int
	 */
	public function interpretStatus() {
		return $this->parseStatus($this->data->getContractStatus());
	}

	/**
	 * @param string $status
	 * 1 - Available
	 * 2 - Sold
	 * 3 - Hold
	 * 4 - Deposit
	 * @return int
	 */
	private function parseStatus($status) {
		syslog(LOG_INFO, "Parsing Status: $status");
		$statusId = 1;

		switch ($status) {
			case "Available":
				break;
			case "Reserved":
				$statusId = 3;
				break;
			case "Rented":
				$statusId = 2;
				break;
			case "Sold":
				$statusId = 2;
				break;
			case "Archived":
				$statusId = 3;
				break;
			case "Prospective":
				$statusId = 3;
				break;
			case "Not Yet Released":
				$statusId = 3;
				break;
			case "Released":
				$statusId = 1;
				break;
			case "Completed":
				$statusId = 2;
				break;
			case "Divested":
				$statusId = 1;
				break;
			case "EOI":
				$statusId = 1;
				break;
			case "Conditional":
				$statusId = 3;
				break;
			case "Unconditional":
				$statusId = 2;
				break;
			case "Settled":
				$statusId = 2;
				break;
			case "Allocated":
				$statusId = 3;
				break;
			case "Unallocated":
				$statusId = 3;
				break;
			case "Not Available":
				$statusId = 3;
				break;
			case "Pending":
				$status = 3;
				break;
			case "Conditional":
				$status = 2;
				break;
			case "Unconditional":
				$status = 2;
				break;
			case "Settled":
				$status = 2;
				break;
			case "Did not meet conditions":
				$status = 1;
				break;
			case "Did not execute contract":
				$status = 1;
				break;
			case "Terminated":
				$status = 1;
				break;
			case "Rescind – Name Change":
				$status = 1;
				break;
			case "Rescind":
				$status = 1;
				break;
			case "EOI":
				$status = 1;
				break;
			case "Hold":
				$status = 3;
				break;
			case "Sold":
				$status = 2;
				break;
			default:
				break;
		}

		syslog(LOG_INFO, "Resulting Status: $statusId");
		return $statusId;
	}
}
