<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\Factory\Builder;

use InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\Factory\IntegrationMessageDataInterfaceFactory;
use InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\Impl\DefaultIntegrationMessageDataInterface;

class DataInterfaceFactoryBuilder {

	/**
	 * @param string $dataInterfaceImplClass
	 * @return IntegrationMessageDataInterfaceFactory
	 */
	public static function generateFactory($dataInterfaceImplClass=DefaultIntegrationMessageDataInterface::class) {
		return new IntegrationMessageDataInterfaceFactory($dataInterfaceImplClass);
	}
}
