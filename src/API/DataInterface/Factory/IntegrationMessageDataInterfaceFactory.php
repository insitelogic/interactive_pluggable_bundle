<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\Factory;

use InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\DataInterface;
use InSiteLogic\Integration\Salesforce\Listing\API\Model\DTO\ListingMessageDTO;

class IntegrationMessageDataInterfaceFactory {

	/**
	 * @var string
	 */
	private $class;

	/**
	 * DataInterfaceFactory constructor.
	 * @param string $class
	 */
	public function __construct($class) { $this->class = $class; }

	/**
	 * @param ListingMessageDTO $listingMessage
	 * @return DataInterface
	 */
	public function createDataInterface(ListingMessageDTO $listingMessage) {
		$result = null;
		try {
			$reflectionClass = new \ReflectionClass($this->class);
			$instance = $reflectionClass->newInstance($listingMessage);
			$result = $instance;
		} catch (\ReflectionException $e) {
			syslog(LOG_INFO, "failed to instantiate new instance");
		}

		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $result;
	}
}
