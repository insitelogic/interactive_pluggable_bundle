<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\Service;

use InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\Factory\IntegrationMessageDataInterfaceFactory;
use InSiteLogic\Integration\Salesforce\Listing\API\Model\DTO\ListingMessageDTO;
use InSiteLogic\Integration\Salesforce\Listing\API\Repository\ListingIntegrationRepository;

class ListingIntegrationService {

	/**
	 * @var ListingIntegrationRepository
	 */
	private $integrationRepository;

	/**
	 * @var IntegrationMessageDataInterfaceFactory
	 */
	private $dataInterfaceFactory;

	/**
	 * ListingIntegrationService constructor.
	 * @param ListingIntegrationRepository $integrationRepository
	 * @param IntegrationMessageDataInterfaceFactory $dataInterfaceFactory
*/
	public function __construct(ListingIntegrationRepository $integrationRepository, IntegrationMessageDataInterfaceFactory $dataInterfaceFactory) {
		$this->integrationRepository = $integrationRepository;
		$this->dataInterfaceFactory = $dataInterfaceFactory;
	}

	public function handleMessage(ListingMessageDTO $message) {
		try {
			$dataInterface 	= $this->dataInterfaceFactory->createDataInterface($message);
			$recordName 	= $dataInterface->getListingName();
			$recordId   	= $this->integrationRepository->LookupRecordByName($recordName);
			$status     	= $dataInterface->interpretStatus();

			return $this->integrationRepository->updateRecordWithStatus($recordId, $status);
		} catch (\Exception $e) {
			syslog(LOG_ERR, $e->getMessage());
			syslog(LOG_ERR, $e->getTraceAsString());
			return null;
		}
	}

	private function parseSalesforceName($name) {
		$rgx = '/^[Ll]ot\s([0-9A-Za-z]+)\s.*$/';
		if(preg_match($rgx, $name, $matches)) {
			return $matches[1];
		} else {
			return null;
		}
	}
}
