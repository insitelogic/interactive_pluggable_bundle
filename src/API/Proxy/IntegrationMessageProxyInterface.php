<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\Proxy;

use InSiteLogic\Util\ISDBAdapter;

interface IntegrationMessageProxyInterface {

	/**
	 * @param ISDBAdapter $dbAdapter
	 * @param int $recordId
	 * @param int $status
	 * @return boolean
	 */
	public function applyIntegrationMessageToDatabase(ISDBAdapter $dbAdapter, $recordId, $status);

	/**
	 * @param ISDBAdapter $dbAdapter
	 * @param string $recordName
	 * @return int
	 */
	public function LookupRecordIdByName(ISDBAdapter $dbAdapter, $recordName);
}
