<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\Controller;

use InSiteLogic\Integration\Salesforce\Listing\API\Model\DTO\ListingMessageDTO;
use InSiteLogic\Integration\Salesforce\Listing\API\Service\ListingIntegrationService;
use InSiteLogic\Util\ObjectHydrator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReceiveIntegrationMessageController {

	/**
	 * @var ListingIntegrationService
	 */
	private $integrationService;

	/**
	 * ReceiveIntegrationMessageController constructor.
	 * @param $integrationService
	 */
	public function __construct($integrationService) { $this->integrationService = $integrationService; }

	/**
	 * @param Request $request
	 * @return Response
	 */
	public function receiveIntegrationMessageAction(Request $request) {
		$payload = $request->request->all();
		syslog(LOG_INFO, print_r($payload, true));
		$listingMessage = ObjectHydrator::getDefaultHydrator()->hydrate($payload, ListingMessageDTO::class);
		$this->integrationService->handleMessage($listingMessage);
		return JsonResponse::create([ 'status' => 'OK' ]);
	}
}
