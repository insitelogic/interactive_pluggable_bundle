<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\Model\DTO;

class ListingMessageDTO {

	/** @var String */
	private $stage_name;

	/** @var String */
	private $project_name;

	/** @var String */
	private $unit_name;

	/** @var String */
	private $frontage;

	/** @var String */
	private $area;

	/** @var String */
	private $name;

	/** @var String */
	private $contract_status;

	/** @var String */
	private $status;

	/** @var String */
	private $available_on_website;

	/** @var String */
	private $not_available_on_website;

	/** @var String */
	private $allowed_on_portal;

	/**
	 * @return String
	 */
	public function getStageName() {
		return $this->stage_name;
	}

	/**
	 * @param String $stage_name
	 */
	public function setStageName($stage_name) {
		$this->stage_name = $stage_name;
	}

	/**
	 * @return String
	 */
	public function getProjectName() {
		return $this->project_name;
	}

	/**
	 * @param String $project_name
	 */
	public function setProjectName($project_name) {
		$this->project_name = $project_name;
	}

	/**
	 * @return String
	 */
	public function getUnitName() {
		return $this->unit_name;
	}

	/**
	 * @param String $unit_name
	 */
	public function setUnitName($unit_name) {
		$this->unit_name = $unit_name;
	}

	/**
	 * @return String
	 */
	public function getFrontage() {
		return $this->frontage;
	}

	/**
	 * @param String $frontage
	 */
	public function setFrontage($frontage) {
		$this->frontage = $frontage;
	}

	/**
	 * @return String
	 */
	public function getArea() {
		return $this->area;
	}

	/**
	 * @param String $area
	 */
	public function setArea($area) {
		$this->area = $area;
	}

	/**
	 * @return String
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param String $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return String
	 */
	public function getContractStatus() {
		return $this->contract_status;
	}

	/**
	 * @param String $contract_status
	 */
	public function setContractStatus($contract_status) {
		$this->contract_status = $contract_status;
	}

	/**
	 * @return String
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @param String $status
	 */
	public function setStatus($status) {
		$this->status = $status;
	}

	/**
	 * @return String
	 */
	public function getAvailableOnWebsite() {
		return $this->available_on_website;
	}

	/**
	 * @param String $available_on_website
	 */
	public function setAvailableOnWebsite($available_on_website) {
		$this->available_on_website = $available_on_website;
	}

	/**
	 * @return String
	 */
	public function getNotAvailableOnWebsite() {
		return $this->not_available_on_website;
	}

	/**
	 * @param String $not_available_on_website
	 */
	public function setNotAvailableOnWebsite($not_available_on_website) {
		$this->not_available_on_website = $not_available_on_website;
	}

	/**
	 * @return String
	 */
	public function getAllowedOnPortal() {
		return $this->allowed_on_portal;
	}

	/**
	 * @param String $allowed_on_portal
	 */
	public function setAllowedOnPortal($allowed_on_portal) {
		$this->allowed_on_portal = $allowed_on_portal;
	}
}
