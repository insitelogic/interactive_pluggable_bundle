<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\Model\Database;

class DBLot {

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $slug;

	/**
	 * @var int
	 */
	private $area;

	/**
	 * @var float
	 */
	private $frontage;

	/**
	 * @var int
	 */
	private $stageId;

	/**
	 * @var int
	 */
	private $status;

	/**
	 * @var boolean
	 */
	private $enabled;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @return int
	 */
	public function getArea() {
		return $this->area;
	}

	/**
	 * @return float
	 */
	public function getFrontage() {
		return $this->frontage;
	}

	/**
	 * @return int
	 */
	public function getStageId() {
		return $this->stageId;
	}

	/**
	 * @return int
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @return bool
	 */
	public function isEnabled() {
		return $this->enabled;
	}
}
