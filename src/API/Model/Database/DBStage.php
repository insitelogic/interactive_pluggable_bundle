<?php

namespace InSiteLogic\Integration\Salesforce\Listing\API\Model\Database;

class DBStage {

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $slug;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var boolean
	 */
	private $enabled;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return bool
	 */
	public function isEnabled() {
		return $this->enabled;
	}
}
