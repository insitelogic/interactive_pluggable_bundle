<?php

namespace InSiteLogic\Integration\Salesforce\Listing\Silex;

use InSiteLogic\Integration\Salesforce\Listing\API\Controller\ReceiveIntegrationMessageController;
use InSiteLogic\Integration\Salesforce\Listing\API\DataInterface\Factory\Builder\DataInterfaceFactoryBuilder;
use InSiteLogic\Integration\Salesforce\Listing\API\Repository\ListingIntegrationRepository;
use InSiteLogic\Integration\Salesforce\Listing\API\Service\ListingIntegrationService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class ListingIntegrationBundleProvider implements ServiceProviderInterface, BootableProviderInterface {

	/**
	 * Bootstraps the application.
	 *
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 *
	 * @param Application $app
	 */
	public function boot(Application $app) {
		$app->post('/api/integration/salesforce', 'controller.listing-integration-controller:receiveIntegrationMessageAction');
	}

	/**
	 * Registers services on the given container.
	 *
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 *
	 * @param Container $pimple A container instance
	 */
	public function register(Container $pimple) {
		$pimple['repository.listing-integration-repository'] = function (Container $pimple) {
			return new ListingIntegrationRepository($pimple['db'], $pimple['integration.delegated-message-proxy']);
		};

		$pimple['service.listing-integration-service'] = function (Container $pimple) {
			// if we do not have a provided factory, use a default one
			if(!$pimple->offsetExists('data-interface.integration-msg-interface-factory')) {
				$dataInterfaceFactory = DataInterfaceFactoryBuilder::generateFactory();
			} else {
				// otherwise, use the provided implementation in our factory
				$dataInterfaceFactory = $pimple['data-interface.integration-msg-interface-factory'];
			}

			return new ListingIntegrationService($pimple['repository.listing-integration-repository'], $dataInterfaceFactory);
		};

		$pimple['controller.listing-integration-controller'] = function (Container $pimple) {
			return new ReceiveIntegrationMessageController($pimple['service.listing-integration-service']);
		};
	}
}
