<?php

require_once __DIR__ . './../vendor/autoload.php';

class ISDBAdapterMock {
	public function queryDBForMultiple($query, array $bindParams, $bindTypeStr = null, $useCache = true) {
		syslog(LOG_INFO, "queryDBForMultiple");
		return [];
	}

	public function queryDBForSingle($query, array $bindParams, $bindTypeStr = null, $useCache = true) {
		syslog(LOG_INFO, "queryDBForSingle");
		return [];
	}

	public function runUpdateQuery($query, array $bindParams, $bindTypeStr = null) {
		syslog(LOG_INFO, "runUpdateQuery");
		return true;
	}
}

class TestDelegatedProxy implements \InSiteLogic\Integration\Salesforce\Listing\API\Proxy\IntegrationMessageProxyInterface {

	/**
	 * @param \InSiteLogic\Util\ISDBAdapter $dbAdapter
	 * @param int $recordId
	 * @param int $status
	 * @return boolean
	 */
	public function applyIntegrationMessageToDatabase($dbAdapter, $recordId, $status) {
		return true;
	}

	/**
	 * @param \InSiteLogic\Util\ISDBAdapter $dbAdapter
	 * @param string $recordName
	 * @return int
	 */
	public function LookupRecordIdByName($dbAdapter, $recordName) {
		return 1;
	}
}

$app = new \Silex\Application();
$app['debug'] = true;

$app->register(new \Silex\Provider\ServiceControllerServiceProvider());
$app->register(new \InSiteLogic\Http\JsonBodyParserServiceProvider());
$app['db'] = function () {
	return new ISDBAdapterMock();
};

$app->register(new \InSiteLogic\Integration\Salesforce\Listing\Silex\ListingIntegrationBundleProvider(), [
	'integration.delegated-message-proxy' => new TestDelegatedProxy()
]);

$app->run();
