# Salesforce/InSite Listing Integration Bundle

### Installing

Within your `index.php` or wherever you initialise Silex, include the following to register all the necessary services into Pimple.

```
$app->register(new \InSiteLogic\Integration\Salesforce\Listing\Silex\ListingIntegrationBundleProvider());
```

### Requirements

- You are using some kind of Middleware that properly handles and parses JSON requests into the Request body field.
    - MicroServiceCore project provides a service provider that suits this requirement: `JsonBodyParserServiceProvider`
- Your relevant database connection is wrapped in a `ISDBAdapter` class available via Pimple on the following Container key: `db`.
- You are using the provided `ServiceControllerServiceProvider` to allow controllers as services.

### Provides
- `/api/integration/salesforce` POST URL for handling Listing messages.
